<?php
/**
 * @author Hampus Aldensten <hampus.aldensten@gmail.com>
 */
class Operator {

    /**
     * @param int $value1
     * @param int $value2
     * @return int
     */
    public function _add($value1, $value2) {
        return $value1 + $value2;
    }

    /**
     * @param int $value1
     * @param int $value2
     * @return int
     */
    protected function _subtract($value1, $value2) {
        return $value1 - $value2;
    }

    /**
     * @param int $value1
     * @param int $value2
     * @return int
     */
    protected function _divide($value1, $value2) {
        if ($value1 == 0 || $value2 == 0) {
            echo 'Can\'t divide by zero.<br>';
            return $value1;
        } else {
            return $value1 / $value2;
        }
    }

    /**
     * @param int $value1
     * @param int $value2
     * @return int
     */
    protected function _multiply($value1, $value2) {
        return $value1 * $value2;
    }

    /**
     * @param int $value1
     * @param int $value2
     * @return int
     */
    protected function _modulus($value1, $value2) {
        /* if ($value2 == 0) {
            echo 'Can\'t modulus by zero.<br>';
            return $value1 % $value2;
        } else { */
            return $value1 % $value2;
        //}
    }

    /**
     * @param int $value1
     * @param int $value2
     * @return int
     */
    protected function _squareRoot($value1, $value2) {
        return sqrt($value1 + $value2);
    }
}