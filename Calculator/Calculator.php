<?php
/**
 * @author Hampus Aldensten <hampus.aldensten@gmail.com>
 */
final class Calculator extends Operator implements CalculatorInterface {
    
    /**
     * @var int $int1
     */
    protected $int1 = 0;

    /**
     * @var int $int2
     */
    private $int2 = 0;

    /**
     * @var string $operator
     */
    public $operator;

    /**
     * @return void
     */
    public function calculate() {
        switch($this->operator) {
            case '+':
                @$_SESSION['result'][] = parent::_add($this->memory(), $this->getValue());
                break;
            case '-':
                @$_SESSION['result'][] = parent::_subtract($this->memory(), $this->getValue());
                break;
            case '/':    
                @$_SESSION['result'][] = parent::_divide($this->memory(), $this->getValue());
                break;
            case '*':
                @$_SESSION['result'][] = parent::_multiply($this->memory(), $this->getValue());
                break;
            case '%':
                @$_SESSION['result'][] = parent::_modulus($this->memory(), $this->getValue());
                break;
            case '√':
                @$_SESSION['result'][] = parent::_squareRoot($this->memory(), $this->getValue());
                break;
        }
    }

    /**
     * @param int $postValue
     * @return void
     */
    public function setValue($postValue) {
        $this->int1 = $postValue;
    }

    /**
     * @return int 
     */
    protected function getValue() {
        return $this->int1;
    }
    
    /**
     * @return int
     */
    private function memory() {
        return $this->int2 = end($_SESSION['result']);
    }
}

