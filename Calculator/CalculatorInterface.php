<?php 
/**
 * @author Hampus Aldensten <hampus.aldensten@gmail.com>
 */
interface CalculatorInterface {
    public function calculate();
    public function setValue($postValue);
}