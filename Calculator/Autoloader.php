<?php
/**
 * @author Hampus Aldensten <hampus.aldensten@gmail.com>
 */
class Autoloader {
    public function __construct() {
        spl_autoload_register(array($this, 'loader'));
    }
    private function loader($className) {
        //echo 'Trying to load ', $className, ' via ', __METHOD__, "()\n<br>";
        include $className . '.php';
    }
}